<?php
session_start();
require_once ('config/root_config.php');
if (isset($_SESSION['room']) && isset($_SESSION['user-details'])){
//    print_v($_SESSION);
}else{
    session_destroy();
    header('location: search');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/css/style2.css">
    <?php require('./_inc/header.php') ?>
    <style>
        button{
            outline: none;
            border: none;
            box-shadow: none;

        }
    </style>
</head>

<body>
    <header id="main-header">
        <a href="./" class="header-link-tag">Hotel Billing System</a>
    </header>
    <main>
        <section class="intro-section">
            <div class="section-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-content">
                                <h4>Checkout</h4>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="nd_booking_section nd_booking_position_relative mb-rems">
                                <ul
                                    class="nd_booking_list_style_none nd_booking_padding_0 nd_booking_margin_0 booking-alignment">
                                    <li id="nd_booking_vc_steps_search"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            1</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase"
                                            href="./search">Search</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_booking"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_bg_custom_color nd_booking_border_1_solid_greydark_important nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            2</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text"
                                            href="#">Booking</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_checkout"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_bg_greydark  nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            3</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text"
                                            href="#">Checkout</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_thankyou"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            4</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text"
                                            href="#">Thank You</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section class="room-section">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <div class="hotel-details">
                            <div class="details-card">
                                <div class="card card-1"></div>
                                <div class="card card-2"></div>
                                <div class="card card-3 mb-0"></div>
                                <div class="card card-4 mb-0"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="booking">
                            <div class="nd_booking_section pb-5">
                                <h1>Confirm Billing :</h1>
                                <div class="nd_booking_section nd_booking_height_30"></div>
                                <form method="post" action="" id="paymentForm">
                                    <div id="nd_booking_booking_form_name_container"
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_right_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p>All your informations have been submitted, please click the button below to proceed.</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                    </div>
                                    <div class="form-details">
                                        <input type="hidden" id="fullName" value="<?php echo $_SESSION['user-details']['name'] ?>">
                                        <input type="hidden" id="email-address" name="email" value="<?php echo $_SESSION['user-details']['email'] ?>">
                                        <input type="hidden" id="contact" name="contact" value="<?php echo $_SESSION['user-details']['telephone'] ?>">
                                        <input type="hidden" id="amount" name="price" value="<?php echo $_SESSION['room']['price'] ?>">
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div
                                        class="nd_booking_width_100_percentage nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <button class="nd_booking_font_size_11 nd_options_second_font_important nd_booking_font_weight_bolder nd_booking_letter_spacing_2 nd_booking_padding_15_35_important"
                                            type="submit" style="background-color: #c19b77; color: #ffffff">
                                            Proceed To Billing
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<!--    <script src="https://js.paystack.co/v1/inline.js"></script>-->
    <script src="./assets/js/paystack.js"></script>
    <script>
        const paymentForm = document.getElementById('paymentForm');
        paymentForm.addEventListener("submit", payWithPaystack, false);
        function payWithPaystack(e) {
            e.preventDefault();
            let handler = PaystackPop.setup({
                key: 'pk_test_b56f295b00759a00ad26144d70a0cb000261334c', // Replace with your public key
                email: document.getElementById("email-address").value,
                contact: document.getElementById("contact").value,
                amount: document.getElementById("amount").value * 100,
                ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                // label: "Optional string that replaces customer email"
                onClose: function(){
                    alert('Window closed.');
                },
                callback: function(response){
                    let message = 'Payment complete! Reference: ' + response.reference;
                    alert(message);
                }
            });
            handler.openIframe();
        }
    </script>
</body>

</html>