<?php
    session_start();
    require_once ('config/root_config.php');
    require_once ('config/booking_config.php');
    $room = null;
    if(isset($_POST['billing']) && !empty($_POST['billing'])){
        $user_details = [
            'name'=> $_POST['first_name'] .' '. $_POST['last_name'],
            'email'=> $_POST['email'],
            'telephone'=> $_POST['telephone'],
            'address'=> $_POST['address'],
            'city'=> $_POST['city'],
            'country'=> $_POST['country'],
            'zip'=> $_POST['zip'] || 'Null',
            'requests'=> $_POST['requests'],
            'arrival'=> $_POST['arrival'],
        ];
        $_SESSION["user-details"] = $user_details;
        header('location: checkout');
        exit();
    }
    if (isset($_POST['room_id']) && !empty($_POST['room_id'])) {
        $room = Booking::getRoom($_POST['room_id']);
        $_SESSION['room'] = $room;
    }else{
        header('location: search');
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/css/style2.css">
    <?php require('./_inc/header.php') ?>
    <style>
        .form-label-margin{
            margin-bottom: 0.2rem;
        }
    </style>
</head>

<body>
    <header id="main-header">
        <h4>School Swep Hotel Billing System</h4>
    </header>
    <main>
        <section class="intro-section">
            <div class="section-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-content">
                                <h4>
                                    Booking
                                </h4>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="nd_booking_section nd_booking_position_relative mb-rems">
                                <ul class="nd_booking_list_style_none nd_booking_padding_0 nd_booking_margin_0 booking-alignment">
                                    <li id="nd_booking_vc_steps_search"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            1</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase"
                                            href="./search">Search</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_booking"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_bg_greydark nd_booking_bg_custom_color nd_booking_border_1_solid_greydark_important nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            2</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text"
                                            href="#">Booking</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_checkout"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            3</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text"
                                            href="#">Checkout</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_thankyou"
                                        class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1
                                            class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            4</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text"
                                            href="#">Thank You</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="room-section">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <div class="hotel-details">
                            <div class="details-card">
                                <div class="card card-1"></div>
                                <div class="card card-2"></div>
                                <div class="card card-3 mb-0"></div>
                                <div class="card card-4 mb-0"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-8">
                        <div class="booking">
                            <div class="nd_booking_section pb-5">
                                <div class="nd_booking_section nd_booking_height_40"></div>
                                <h1>Add Your Informations :</h1>
                                <div class="nd_booking_section nd_booking_height_30"></div>
                                <form method="post" action="<?=$_SERVER['PHP_SELF'];?>">
                                    <div id="name_container"
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_right_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">First Name *</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="first_name"
                                            name="first_name" type="text" value="Emmanuel">
                                    </div>
                                    <div id="surname_container"
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_left_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Last Name *</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="last_name"
                                            name="last_name" type="text" value="Popoola">
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div id="email_container"
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_right_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Email *</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="email"
                                            name="email" type="text" value="0.emmanuelpopoola@gmail.com">
                                    </div>
                                    <div id="phone_container"
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_left_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Telephone *</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="phone"
                                            name="telephone" type="text" value="08032623369">
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_right_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Address</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="address"
                                            name="address" type="text" value="Shalom Hostel, Under G">
                                    </div>
                                    <div
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_left_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">City</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="city"
                                            name="city" type="text" value="Ogbomoso">
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_right_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Country</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="country"
                                            name="country" type="text" value="Nigeria">
                                    </div>
                                    <div
                                        class="nd_booking_width_50_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone nd_booking_padding_left_10 nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">ZIP</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <input required class="nd_booking_section" id="zip"
                                            name="zip" type="text" value="200210">
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div id="requests_container"
                                        class="nd_booking_width_100_percentage nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Requests</p>
                                        <div class="nicdark_section nicdark_height_5"></div>
                                        <textarea class="nd_booking_section" id="requests"
                                            rows="6" name="requests">Neat Room</textarea>
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div
                                        class=" nd_booking_width_100_percentage nd_booking_width_100_percentage_all_iphone nd_booking_padding_0_all_iphone  nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="form-label-margin">Arrival</p>
                                        <select class="nd_booking_section" name="arrival"
                                            id="arrival">
                                            <option>I do not know</option>
                                            <option selected>12:00 - 1:00 am</option>
                                            <option>1:00 - 2:00 am</option>
                                            <option>2:00 - 3:00 am</option>
                                            <option>3:00 - 4:00 am</option>
                                            <option>4:00 - 5:00 am</option>
                                            <option>5:00 - 6:00 am</option>
                                            <option>6:00 - 7:00 am</option>
                                            <option>7:00 - 8:00 am</option>
                                            <option>8:00 - 9:00 am</option>
                                            <option>9:00 - 10:00 am</option>
                                            <option>10:00 - 11:00 am</option>
                                            <option>11:00 - 12:00 am</option>
                                            <option>12:00 - 1:00 pm</option>
                                            <option>1:00 - 2:00 pm</option>
                                            <option>2:00 - 3:00 pm</option>
                                            <option>3:00 - 4:00 pm</option>
                                            <option>4:00 - 5:00 pm</option>
                                            <option>5:00 - 6:00 pm</option>
                                            <option>6:00 - 7:00 pm</option>
                                            <option>7:00 - 8:00 pm</option>
                                            <option>8:00 - 9:00 pm</option>
                                            <option>9:00 - 10:00 pm</option>
                                            <option>10:00 - 11:00 pm</option>
                                            <option>11:00 - 12:00 pm</option>
                                        </select>
                                    </div>
                                    <input  type="hidden" style="display: none" name="billing" value="true">
                                    <div class="nd_booking_section nd_booking_height_20  "></div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div id="term_container"
                                        class="nd_booking_width_100_percentage nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <p class="nd_booking_margin_0 nd_booking_section form-label-margin">
                                            <input required
                                                class="nd_booking_float_left nd_booking_margin_top_8 nd_booking_margin_right_10"
                                                id="term" name="term"
                                                type="checkbox" checked="" value="1">
                                            <a class="nd_booking_float_left" target="_blank"
                                                href="javascript:void(0)">Terms
                                                and
                                                conditions *</a>
                                        </p>
                                    </div>
                                    <div class="nd_booking_section nd_booking_height_20"></div>
                                    <div
                                        class="nd_booking_width_100_percentage nd_booking_box_sizing_border_box nd_booking_float_left">
                                        <input id="nd_booking_submit_go_to_checkout"
                                               style="background-color: #c19b77; color: #ffffff"
                                            class="nd_booking_font_size_11 nd_options_second_font_important nd_booking_font_weight_bolder nd_booking_letter_spacing_2 nd_booking_padding_15_35_important"
                                            type="submit" value="CHECK-IN">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>

</html>