<?php
require_once('root_config.php');

class Rooms
{
    protected static $dsn = 'mysql: host=' . HOST . '; dbname=' . DBNAME;
    protected static $options = [
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];

    public static function getRooms()
    {
        try {
            $dbh = new PDO(self::$dsn, DBUSER, DBPASS, self::$options);
            $stm = $dbh->prepare('SELECT * FROM ' . 'room' . ' ORDER BY id DESC LIMIT 20');
            $stm->execute();

            if ($stm->rowCount() > 0) {
                return $stm->fetchAll();
            } else {
                return null;
            }
        } catch (PDOException $e) {
            print "Error!:" . $e->getMessage() . "</br>";
        }
        return  null;
    }

    public static function getOffset($tableName)
    {
        try {
            $dbh = new PDO(self::$dsn, DBUSER, DBPASS, self::$options);

            $stm = $dbh->prepare('SELECT * FROM ' . $tableName . ' ORDER BY id DESC LIMIT 5, 10');

            $stm->execute();

            if ($stm->rowCount() > 0) {
                return $stm->fetchAll();
            } else {
                return null;
            }
        } catch (PDOException $e) {
            print "Error!:" . $e->getMessage() . "</br>";
        }
    }
}
