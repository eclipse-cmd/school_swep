<?php
require_once ('config/root_config.php');
require_once ('config/rooms_config.php');
$rooms = Rooms::getRooms();
function getGuest($guest){

}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/css/style2.css">
    <?php require('./_inc/header.php') ?>
</head>

<body>
    <header id="main-header">
        <a href="./" class="header-link-tag">Hotel Billing System</a>
    </header>
    <main>
        <section class="intro-section">
            <div class="section-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-content">
                                <h4> Search </h4>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="nd_booking_section nd_booking_position_relative mb-rems">
                                <ul class="nd_booking_list_style_none nd_booking_padding_0 nd_booking_margin_0 booking-alignment">
                                    <li id="nd_booking_vc_steps_search" class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1 class=" nd_booking_border_1_solid_white nd_booking_bg_greydark nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            1</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase" href="javascript:void(0)">Search</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_booking" class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1 class=" nd_booking_border_1_solid_white nd_booking_bg_custom_color nd_booking_border_1_solid_greydark_important nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            2</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text" href="#">Booking</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_checkout" class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1 class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            3</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text" href="#">Checkout</a>
                                    </li>
                                    <li id="nd_booking_vc_steps_thankyou" class="nd_booking_display_inline_block nd_booking_margin_right_20 nd_booking_margin_left_20">
                                        <h1 class=" nd_booking_border_1_solid_white nd_booking_width_20 nd_booking_height_20 nd_booking_line_height_20 nd_booking_font_size_11 nd_booking_display_inline_block nd_booking_border_radius_100_percentage nd_options_color_white nd_booking_margin_right_10 text-center">
                                            4</h1>
                                        <a class="nd_options_color_white nd_booking_font_size_12 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase nd_booking_cursor_text" href="#">Thank You</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="room-section">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <div class="hotel-details">
                            <div class="details-card">
                                <div class="card card-1"></div>
                                <div class="card card-2"></div>
                                <div class="card card-3 mb-0"></div>
                                <div class="card card-4 mb-0"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="rooms">
                            <div class="row">
                                <?php foreach ($rooms as $room) : ?>
                                    <div class="col-6 mb-5">
                                        <div class="nd_booking_section nd_booking_border_1_solid_grey nd_booking_bg_white">
                                            <div class="nd_booking_section nd_booking_position_relative">
                                                <img alt="" class="nd_booking_section"
                                                     src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/uploads/sites/2/2017/06/room-1-1024x664.jpg">
                                                <div class="nd_booking_bg_greydark_alpha_gradient_3 nd_booking_position_absolute nd_booking_left_0 nd_booking_height_100_percentage nd_booking_width_100_percentage nd_booking_padding_30 nd_booking_box_sizing_border_box">
                                                    <div class="nd_booking_position_absolute nd_booking_bottom_20">
                                                        <p class="nd_options_color_white nd_booking_margin_right_10 nd_booking_float_left nd_booking_font_size_11 nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase">
                                                            Hotel Rome
                                                        </p>
                                                        <img alt="" class="nd_booking_margin_right_5 nd_booking_float_left" width="10"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/plugins/nd-booking/assets/img/icons/icon-star-full-white.svg"><img
                                                            alt="" class="nd_booking_margin_right_5 nd_booking_float_left"
                                                            width="10"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/plugins/nd-booking/assets/img/icons/icon-star-full-white.svg"><img
                                                            alt="" class="nd_booking_margin_right_5 nd_booking_float_left"
                                                            width="10"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/plugins/nd-booking/assets/img/icons/icon-star-full-white.svg">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="nd_booking_section nd_booking_padding_30 nd_booking_box_sizing_border_box">
                                                <a href="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/rooms/small-room/">
                                                    <h1 class="text-capitallize"><?php echo $room['room_type'] ?></h1>
                                                </a>
                                                <div class="nd_booking_section nd_booking_height_10"></div>
                                                <div class="nd_booking_section">
                                                    <div class="nd_booking_display_table nd_booking_float_left">
                                                        <img alt="" class="nd_booking_margin_right_10 nd_booking_display_table_cell nd_booking_vertical_align_middle"
                                                             width="23"
                                                             src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/plugins/nd-booking/assets/img/icons/icon-user-grey.svg">
                                                        <p class="  nd_booking_display_table_cell nd_booking_vertical_align_middle nd_booking_font_size_12 nd_booking_line_height_26">
                                                            1 GUESTS</p>
                                                        <img alt=""
                                                             class="nd_booking_margin_right_10 nd_booking_margin_left_20 nd_booking_display_table_cell nd_booking_vertical_align_middle"
                                                             width="20"
                                                             src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/plugins/nd-booking/assets/img/icons/icon-plan-grey.svg">
                                                        <p class="  nd_booking_display_table_cell nd_booking_vertical_align_middle nd_booking_font_size_12 nd_booking_line_height_26">
                                                            15 Ft²</p>
                                                    </div>
                                                </div>
                                                <p><?php echo $room['description'] ?></p>
                                                <div class="nd_booking_section nd_booking_height_20"></div>
                                                <form method="post" action="./booking">
                                                    <input type="hidden" name="room_id" value="<?php echo $room['id']?>">
                                                    <input style="border:2px solid #1c1c1c; color:#1c1c1c;"
                                                           class="nd_booking_padding_15_30_important nd_options_second_font_important nd_booking_border_radius_0_important nd_booking_background_color_transparent_important nd_booking_cursor_pointer nd_booking_display_inline_block nd_booking_font_size_11 nd_booking_font_weight_bold nd_booking_letter_spacing_2"
                                                           type="submit" value="BOOK NOW FOR #<?php echo $room['price']?>">
                                                </form>
                                                <div class="nd_booking_section nd_booking_height_20"></div>
                                                <div class="nd_booking_section nd_booking_height_1 nd_booking_border_bottom_1_solid_grey"></div>
                                                <div class="nd_booking_section nd_booking_height_20"></div>
                                                <a title="Swimming Pool" class="nd_booking_tooltip_jquery nd_booking_float_left">
                                                    <img alt="Swimming Pool"
                                                            class="nd_booking_margin_right_15 nd_booking_float_left" width="23"
                                                            height="23"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/wp-content/uploads/2017/07/icon-14-1.png">
                                                </a>
                                                <a title="Television" class="nd_booking_tooltip_jquery nd_booking_float_left">
                                                    <img
                                                            alt="Television"
                                                            class="nd_booking_margin_right_15 nd_booking_float_left" width="23"
                                                            height="23"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/wp-content/uploads/2017/07/icon-18.png">
                                                </a>
                                                <a title="No Smoking" class="nd_booking_tooltip_jquery nd_booking_float_left">
                                                    <img
                                                            alt="No Smoking"
                                                            class="nd_booking_margin_right_15 nd_booking_float_left" width="23"
                                                            height="23"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/wp-content/uploads/2017/07/icon-14.png">
                                                </a>
                                                <a title="Private Bathroom" class="nd_booking_tooltip_jquery nd_booking_float_left">
                                                    <img
                                                            alt="Private Bathroom"
                                                            class="nd_booking_margin_right_15 nd_booking_float_left" width="23"
                                                            height="23"
                                                            src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/wp-content/uploads/2017/07/icon-10.png">
                                                </a>
                                                <a href="javascript:void(0)"
                                                   class="nd_booking_margin_top_7 nd_booking_margin_top_20_all_iphone nd_booking_width_100_percentage_all_iphone nd_booking_float_right nd_booking_float_left_all_iphone nd_booking_display_inline_block nd_booking_text_align_center nd_booking_box_sizing_border_box nd_booking_font_size_12">
                                                <span class="nd_booking_float_left nd_booking_font_size_11 nd_booking_letter_spacing_2">FULL
                                                    INFO</span>
                                                    <img alt="" class="nd_booking_margin_left_5 nd_booking_float_left"
                                                         width="10"
                                                         src="http://www.nicdarkthemes.com/themes/hotel/wp/demo/hotel/wp-content/plugins/nd-booking/assets/img/icons/icon-right-arrow-grey.svg">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>

</html>