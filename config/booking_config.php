<?php
require_once('root_config.php');

class Booking
{
    protected static $dsn = 'mysql: host=' . HOST . '; dbname=' . DBNAME;
    protected static $options = [
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];

    public static function getRoom($id)
    {
        try {
            $dbh = new PDO(self::$dsn, DBUSER, DBPASS, self::$options);
            $stm = $dbh->prepare('SELECT * FROM room  WHERE id = ?');
            $stm->execute([$id]);

            if ($stm->rowCount() == 1) {
                return $stm->fetch();
            } else {
                header('location: booking');
            }
        } catch (PDOException $e) {
            print "Error!:" . $e->getMessage() . "</br>";
        }
        return  null;
    }

}
