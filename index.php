<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <?php require('./_inc/header.php') ?>
</head>

<body>

<header id="main-header">
    <h4>Hotel Billing System</h4>
</header>

<main>
    <section id="hero-section" class="section">
        <div class="section-wrapper hero-section-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-content">
                            <h4>
                                ENJOY A LUXURY <br>
                                EXPERIENCE
                            </h4>
                            <a href="./search.php" class="hero-cta">Book now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

</body>

</html>