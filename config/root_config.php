<?php
define('ROOT', 'localhost/school_swep/');
define('HOST', 'localhost');
define('DBUSER', 'root');
define('DBPASS', '11223');
define('DBNAME', 'school_swep');
$db_arrtributes = [
    PDO::ATTR_EMULATE_PREPARES   => false,
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];

function print_v($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    exit;
}

function print_e($var)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    exit;
}